[![Front](https://img.shields.io/badge/Test-Postman-orange?style=flat)](https://)
[![Front](https://img.shields.io/badge/API-NodeJS-green?style=flat)](https://)

<br/>
<br/>
<div align="center">
    <img src="./images/API-Testing-Postman.png" alt="Logo" width="60%">
    <br/>
    <br/>
    <h3 align="center">Variables, Automation, CI</h3>
</div>

<br />
<br />


<br/>
<br/>



## 🗒 Description
Practical project to improve API Testing with Postman
- Basics functionalities (collections, variables, mock ...)
- Automation
- Continue Integration

<br/>
<br/>


## 🛠 Libraries
* **Ajv** => testing JSON schema
  <details close="close">
    <summary>Import and use</summary>

    ```javascript
    const Ajv = require('ajv');
    const ajv = new Ajv({ logger:console, allErrors:true })
    const schema = {
        type: "object",
        required: [
            "id",
            "size"
        ],
        properties: {
            id: {
                type: "number"
            },
            surname: {
                type: "string"
            },
            size: {
                type: "number"
            }
        }
    }
    ```
 </details>

* **Newman** => To run collections with one command
  <details close="close">
    <summary>Install and use</summary>

    * Export collections
    * Run json file with newman
    ```bash 
    npm install -g newman
    newman run <jsonFileCollection>
    ```
    OR 
    * Share => ViaAPI
    * Run this url  
    ```bash 
    newman run <thisURL>
    ```
    -e : define the environment (export environment before)<br>
    -k : disables SSL verification checks
    ``` 
    newman run <jsonFileCollection> -e <jsonFileEnvironment> -k
    ```
    ![Newman illustration](./images/image4.png)

    * Reporting => `-r cli, json, junit`
    * Setup test script in package.json
    ```javascript
    "scripts": {
        "test": "newman run postmanCourse.postman_collection.json -e FormationPostman_01.Dev.postman_environment.json -k -r cli, json, junit",
        "test:staging": "newman run postmanCourse.postman_collection.json -e FormationPostman_01.Dev.postman_environment.json -k"
  },
    ```

 </details>
  
    
</details>

<br/>
<br/>

## ⚙️ Postman basics
<details close="close">
<summary>First tests</summary>

![Screenshot](./images/image1.png)
</details>

<details close="close">
<summary>Variable - Scope Global</summary>

![Screenshot](./images/image2.png)
How to Get the variable :
```
const value = pm.globals.get('key');
```

</details>

<details close="close">
<summary>Variable - Scope Collection</summary>

![Screenshot](./images/image2.png)
How to Get the variable :
```
pm.collectionVariables.set('myVariable', 150);
const value = pm.collectionVariables.get('myVariable');
console.warn('value', value);
```
</details>

<details close="close">
<summary>Variable - Scope Environment</summary>

New => Ennvironment 
![Screenshot](./images/image3.png)
How to Get the variable :
```
pm.environment.set('key', 15);
const key = pm.environment.get('key');
console.warn('key', key);
```

</details>

<details close="close">
<summary>Variable - Get the overloaded</summary>

```
pm.variables.get('key');
```

</details>

<br/>
<br/>

## ⚙️ Postman - Automation 
* Create a workspace
* Definition of environments
  * Dev
  * Staging
  * Production
* Définition of URL variables for each one
* Create collections 
  * Requests (POST, GET, PUT, DELETE ...)


<br/>
<br/>

## ⚙️ Postman - CI
* Create yaml file .gitlab-cy.yml