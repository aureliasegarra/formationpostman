const express = require('express');
const app = express();
const port = 3000;

app.use(express.urlencoded({ extended: true }));
app.use(express.json());

app.get('/', (req, res) => res.send('API is running ...🥳'));
app.post('/', (req, res) => res.send("OK"));

app.get('/wookies', (req, res) => {
    res.send("OK");
});

app.get('/wookie/:id', (req, res) => {
  
    res.statusCode = 200;
    res.send({
        id: parseInt(req.params.id),
        surname: "Chewie" + req.params.id,
        size: 270,
        data: [
            {
                key: "weapon",
                value: req.query.key
            }
        ]
    });
});

app.post('/login', (req, res) => {

    const { login, password, token} = req.body;

    // Vérification des informations de connexion (ici, on vérifie juste que le username et le password sont non vides)
    if (!login || !password) {
        res.status(400).json({ message: 'Veuillez fournir un nom d\'utilisateur et un mot de passe valides.' });
        return;
    }

    res.status(200).json({ login, password, token });
});

app.get('/selfies', (req, res) => {
    const selfies = [
        "Chewie", "Chewa", "Chewu"
    ]
    res.send(selfies);
});

app.listen(port, () => console.log(`App listening at http://localhost:${port}`));